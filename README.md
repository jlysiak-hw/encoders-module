# Encoder module

Simple double encoder module designed to be small and cheap for manufacture.
It costs $4.8 for 3 proto boards in OSHPark.


Top:

![top](imgs/encoder-board-top.png)

Bottom:

![bot](imgs/encoder-board-bot.png)

